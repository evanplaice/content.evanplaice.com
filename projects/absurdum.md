Absurdum exists as a proof-of-concept. That any and every functional operator typically found in the JS ecosystem (ex lodash) can be implemented using only the reduce function.

In addition, the library provides a clean slate to test if it's feasible to migrate development to vanilla ES. To work, this will require the full specification and implementation of ES modules in both Node.js and Browsers.

<div class="grid">
  <div>
    <h3>Development</h3>
    <ul>
      <li>array operators</li>
      <li>object operators</li>
      <li>string operators</li>
      <li>HTML element operators</li>
      <li>developed following TDD</li>
      <li>CI/CD fully automated</li>
    </ul>
  </div>
  <div>
    <h3>Stack</h3>
    <ul>
      <li>ES-Next - Node.js + Browser</li>
      <li>Tape.js</li>
      <li>GitLab</li>
      <li>Git</li>
    </ul>
  </div>
</div>

**Usage:**
```javascript
# return an array in reverse order
let result = arrays.reverse([1, 2, 3, 4]);
> [4, 3, 2, 1]

// EndsWith tests a string to see if it ends with a substring
let result = strings.endsWith('This sentence ends with', 'with');
console.log(result);
> true
```

**Motivations:**

Work was slow, feast or famine cycles are common in the military. I attempted to pitch a MVP to the president of my company to replace our existing (terrible) logistics tracking system. The concept would provide a web-based UI with a PostgreSQL back-end.

To enable the quick/efficient import/export of tabular data would require a robust CSV parser. I probably searched and read over 100+ different blogs/websites in search of a good example. The results, everybody sucks at parsing CSV and the JS dev community is riddled with amateurs who love ridiculous titles such as 'guru' or 'ninja'.

I received push back on my proposal in the form of, and I quote "we don't want to get involved with technology." I had time to burn (ie I can't stand being idle) so I dropped the product MVP and redirected my focus on creating something useful for others. My goal, to write the first fully RFC compliant CSV parser for client-side CSV. 

It began with the poor assumption, 'RegEx *should* be capable of parsing CSV'. I found a 20 line monster on Stack Overflow and tweaked it for extensibility. Only to discover that -- by design -- the RegEx parser will always break on values that contain escaped newline characters.

Back to the drawing board. I read everything I could find about parsers, Chomsky types, EBNF (Extended Backus-Naur Form), parser generators, finite state machines, etc. Then, and only then, I sat down and mapped out every single state and transition as a DFA (Deterministic Finite-State Automata). The foundation that would become the new feature-complete CSV parser.

**Outcome:**

The popularity of the library exploded with a little marketing help. Piggybacking on jquery didn't hurt either. Ironically, the library doesn't actually depend on jQuery.

Tons of people started using it so it took very little time for users to root out most of the bugs. It was really an intrinsically rewarding experience to have something I created be adopted by so many.

Anyway, the feast-or-famine pace at work switched directions and I left the project on autopilot. The project is mostly dead now. GoogleCode shutdown and the migration to GitHub left years of valuable history behind. In retrospect, I wish I had tried GitHub sooner. The collaborative nature of forks/pull-requests is simply... better.

I love writing code in Javascript. It pained me to see so many self-proclaimed 'JS Ninjas' churning out bullshit, misinformation, and low quality code. It took some effort to 'do things right' but I like to think that my work helped to raise the bar just a little.

To date, the project has had almost 500K downloads. Competing CSV parser libraries have cropped up since and I'm happy to say they all target [RFC 4180][rfc-4180] compliance as the baseline.